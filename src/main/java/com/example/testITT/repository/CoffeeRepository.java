package com.example.testITT.repository;

import com.example.testITT.repository.model.EventLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

interface CoffeeJPARepository extends JpaRepository<EventLog, Long> {
    EventLog findFirstByOrderByCreatedAtDesc();
}

@Repository
public class CoffeeRepository {

    @Autowired
    private CoffeeJPARepository jpaRepo;

    @Transactional
    public EventLog create(EventLog log) {
        return jpaRepo.save(log);
    }

    @Transactional
    public EventLog getLastLog() {
        return jpaRepo.findFirstByOrderByCreatedAtDesc();
    }
}
