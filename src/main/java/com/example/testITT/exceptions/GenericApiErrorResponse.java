package com.example.testITT.exceptions;

import lombok.Getter;
import lombok.Setter;

public class GenericApiErrorResponse {
    @Getter
    @Setter
    String message;

    public GenericApiErrorResponse(String message) {
        this.message = message;
    }
}
