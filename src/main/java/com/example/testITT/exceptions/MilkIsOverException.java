package com.example.testITT.exceptions;

import org.springframework.core.NestedRuntimeException;

public class MilkIsOverException  extends NestedRuntimeException {

    public MilkIsOverException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public MilkIsOverException(String msg) {
        super(msg);
    }

    public MilkIsOverException() {
        super("Milk is over");
    }
}
