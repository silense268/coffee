package com.example.testITT.config;

import com.example.testITT.TestIttApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@Configuration
@EnableCaching
@EnableJpaRepositories(basePackages = "com.example.testITT.repository")
@EntityScan("com.example.testITT.repository.model")
public class JPAConfig {

}
