package com.example.testITT.rest.service;

import com.example.testITT.exceptions.MilkIsOverException;
import com.example.testITT.exceptions.WaterIsOverException;
import com.example.testITT.exceptions.СoffeeIsOverException;
import com.example.testITT.repository.model.EventLog;
import com.example.testITT.rest.model.CoffeeRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CoffeeServiceTest {

    CoffeeService coffeeService = new CoffeeService();

    @Test
    void testCheckOversMilkException(){

        EventLog log = new EventLog();
        log.setLeftoverCoffee(60);
        log.setLeftoverMilk(70);
        log.setLeftoverWater(90);
        CoffeeRequest request = new CoffeeRequest();
        request.setCoffeeConsumption(50);
        request.setMilkConsumption(400);
        request.setWaterConsumption(30);
        Assertions.assertThrows(MilkIsOverException.class, () -> coffeeService.checkOvers(request, log));

    }

    @Test
    void testCheckOversCoffeeException(){

        EventLog log = new EventLog();
        log.setLeftoverCoffee(60);
        log.setLeftoverMilk(70);
        log.setLeftoverWater(90);
        CoffeeRequest request = new CoffeeRequest();
        request.setCoffeeConsumption(500);
        request.setMilkConsumption(40);
        request.setWaterConsumption(30);
        Assertions.assertThrows(СoffeeIsOverException.class, () -> coffeeService.checkOvers(request, log));

    }

    @Test
    void testCheckOversWaterException(){

        EventLog log = new EventLog();
        log.setLeftoverCoffee(60);
        log.setLeftoverMilk(70);
        log.setLeftoverWater(90);
        CoffeeRequest request = new CoffeeRequest();
        request.setCoffeeConsumption(50);
        request.setMilkConsumption(40);
        request.setWaterConsumption(300);
        Assertions.assertThrows(WaterIsOverException.class, () -> coffeeService.checkOvers(request, log));

    }

    @Test
    void generateNewLog(){

        EventLog log = new EventLog();
        log.setLeftoverCoffee(60);
        log.setLeftoverMilk(70);
        log.setLeftoverWater(90);
        CoffeeRequest request = new CoffeeRequest();
        request.setCoffeeConsumption(50);
        request.setMilkConsumption(40);
        request.setWaterConsumption(30);
        EventLog newlog = new EventLog();
        newlog.setLeftoverCoffee(10);
        newlog.setLeftoverMilk(30);
        newlog.setLeftoverWater(60);
        newlog.setEventType(EventLog.EventType.MAKE_COFFEE_WITH_MILK);
        Assertions.assertEquals(newlog, coffeeService.generateNewLog(request, log));
        
    }


}