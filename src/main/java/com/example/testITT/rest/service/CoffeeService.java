package com.example.testITT.rest.service;

import com.example.testITT.exceptions.LogTroubleException;
import com.example.testITT.exceptions.MilkIsOverException;
import com.example.testITT.exceptions.WaterIsOverException;
import com.example.testITT.exceptions.СoffeeIsOverException;
import com.example.testITT.repository.CoffeeRepository;
import com.example.testITT.repository.model.EventLog;
import com.example.testITT.rest.model.CoffeeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NestedRuntimeException;
import org.springframework.stereotype.Service;


@Service
public class CoffeeService {

    @Autowired
    private CoffeeRepository coffeeRepository;

    public EventLog makeCoffee(CoffeeRequest request){

        EventLog lastLog = coffeeRepository.getLastLog();
        EventLog newEvent = generateNewLog(request, lastLog);
        return coffeeRepository.create(newEvent);

    }

    public EventLog addCoffee(){

        EventLog lastLog = createLastLog();
        if (lastLog.getLeftoverCoffee() >= 100) {
            throw new СoffeeIsOverException("Coffee is full");
        }
        lastLog.setEventType(EventLog.EventType.ADD_COFFEE);
        lastLog.setLeftoverCoffee(100);
        return coffeeRepository.create(lastLog);

    }

    public EventLog addMilk(){

        EventLog lastLog = createLastLog();
        if(lastLog.getLeftoverMilk() >= 100){
            throw new WaterIsOverException("Milk is full");
        }
        lastLog.setEventType(EventLog.EventType.ADD_MILK);
        lastLog.setLeftoverMilk(100);
        return coffeeRepository.create(lastLog);

    }

    public EventLog addWater(){

        EventLog lastLog = createLastLog();
        if(lastLog.getLeftoverWater() >= 100){
            throw new WaterIsOverException("Water is full");
        }
        lastLog.setEventType(EventLog.EventType.ADD_WATER);
        lastLog.setLeftoverWater(100);
        return coffeeRepository.create(lastLog);

    }

     protected EventLog generateNewLog(CoffeeRequest request, EventLog last){

        checkOvers(request, last);
        EventLog newLog = new EventLog();
        newLog.setLeftoverCoffee(last.getLeftoverCoffee() - request.getCoffeeConsumption());
        newLog.setLeftoverWater(last.getLeftoverWater() - request.getWaterConsumption());
        newLog.setLeftoverMilk(last.getLeftoverMilk() - request.getMilkConsumption());
        if(request.getMilkConsumption() > 0 ){
            newLog.setEventType(EventLog.EventType.MAKE_COFFEE_WITH_MILK);
        } else {
            newLog.setEventType(EventLog.EventType.MAKE_COFFEE_WITHOUT_MILK);
        }
        return  newLog;

    }

     void checkOvers(CoffeeRequest request, EventLog last){

        if(last.getLeftoverCoffee() - request.getCoffeeConsumption() < 0){
            throw new СoffeeIsOverException("Need more coffee");
        }
        if(last.getLeftoverWater() - request.getWaterConsumption() < 0){
            throw new WaterIsOverException("Need more water");
        }
        if(last.getLeftoverMilk() - request.getMilkConsumption() < 0){
            throw new MilkIsOverException("Need more milk");
        }

    }

    EventLog createLastLog(){
        EventLog lastLog = coffeeRepository.getLastLog();
        if(lastLog == null){
            throw new LogTroubleException();
        }
        return lastLog;
    }


}
