package com.example.testITT.exceptions;

import org.springframework.core.NestedRuntimeException;

public class LogTroubleException extends NestedRuntimeException {

    public LogTroubleException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public LogTroubleException(String msg) {
        super(msg);
    }

    public LogTroubleException() {
        super("Log is not working");
    }
}
