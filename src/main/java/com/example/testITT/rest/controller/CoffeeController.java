package com.example.testITT.rest.controller;

import com.example.testITT.repository.model.EventLog;
import com.example.testITT.rest.model.CoffeeRequest;
import com.example.testITT.rest.service.CoffeeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/v3/coffee")
@Tag(name = "Coffee maker", description = "allows you to brew coffee")
public class CoffeeController {

    @Autowired
    CoffeeService coffeeService;

    @Operation(
            summary = "Give me a coffee please"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Coffee is ready",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Invalid coffee type",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Page not found",
                    content = @Content)})

    @PostMapping(
            value = "/",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public EventLog makeCoffee(
            @Parameter(description = "size - the amount of coffee in milligrams ")
            @Valid
            @RequestBody()
            CoffeeRequest request
    ) {
        return coffeeService.makeCoffee(request);
    }

    @Operation(
            summary = "Add milk"
    )

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Milk is added",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Something wrong",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Page not found",
                    content = @Content)})


    @PostMapping("/addMilk")
    public EventLog addMilk() {
        return coffeeService.addMilk();
    }

    @Operation(
            summary = "Add coffee"
    )

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Milk is added",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Something wrong",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Page not found",
                    content = @Content)})

    @PostMapping("/addCoffee")
    public EventLog addCoffee() {
        return coffeeService.addCoffee();
    }

    @Operation(
            summary = "Add water"
    )

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Milk is added",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Something wrong",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Page not found",
                    content = @Content)})

    @PostMapping("/addWater")
    public EventLog addWater() {
        return coffeeService.addWater();
    }
}
