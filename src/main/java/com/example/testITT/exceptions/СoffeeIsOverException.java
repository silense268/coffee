package com.example.testITT.exceptions;

import org.springframework.core.NestedRuntimeException;

public class СoffeeIsOverException extends NestedRuntimeException {

    public СoffeeIsOverException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public СoffeeIsOverException(String msg) {
        super(msg);
    }

    public СoffeeIsOverException() {
        super("Coffee is over");
    }

}
