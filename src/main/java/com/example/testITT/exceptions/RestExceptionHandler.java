package com.example.testITT.exceptions;

import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice(basePackages = "com.example.testITT")
class RestExceptionHandler {

    @ExceptionHandler({СoffeeIsOverException.class, LogTroubleException.class,
            WaterIsOverException.class, MilkIsOverException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    protected GenericApiErrorResponse handleResourceNotFoundException(NestedRuntimeException ex) {
        return new GenericApiErrorResponse(ex.getMessage());
    }
}
