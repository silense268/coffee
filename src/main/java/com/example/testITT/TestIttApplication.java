package com.example.testITT;

import com.example.testITT.rest.service.CoffeeService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestIttApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestIttApplication.class, args);
	}

}
