package com.example.testITT.repository.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "event_log")
public class EventLog {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false, nullable = false)
    @Getter
    private Long id;

    @Column(name = "event_type")
    @Getter
    @Setter
    private EventType eventType;

    @Column(name = "leftover_coffee")
    @Getter
    @Setter
    private int leftoverCoffee = 0;

    @Column(name = "leftover_milk")
    @Getter
    @Setter
    private int leftoverMilk = 0;

    @Column(name = "leftover_water")
    @Getter
    @Setter
    private int leftoverWater = 0;

    @Column(name = "created_at", nullable = false)
    @Getter
    @Setter
    private ZonedDateTime createdAt = ZonedDateTime.now();

    @Override
    public String toString() {
        return "EventLog{" +
                "id=" + id +
                ", eventType=" + eventType +
                ", leftoverCoffee=" + leftoverCoffee +
                ", leftoverMilk=" + leftoverMilk +
                ", createdAt=" + createdAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventLog eventLog = (EventLog) o;
        return leftoverCoffee == eventLog.leftoverCoffee && leftoverMilk == eventLog.leftoverMilk
                && leftoverWater == eventLog.leftoverWater && Objects.equals(id, eventLog.id) && eventType == eventLog.eventType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, eventType, leftoverCoffee, leftoverMilk, leftoverWater, createdAt);
    }

    public enum EventType {
        INIT,
        MAKE_COFFEE_WITH_MILK,
        MAKE_COFFEE_WITHOUT_MILK,
        ADD_COFFEE,
        ADD_MILK,
        ADD_WATER,
    }

}
