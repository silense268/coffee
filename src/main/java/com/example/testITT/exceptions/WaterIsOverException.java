package com.example.testITT.exceptions;

import org.springframework.core.NestedRuntimeException;

public class WaterIsOverException extends NestedRuntimeException {

    public WaterIsOverException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public WaterIsOverException(String msg) {
        super(msg);
    }

    public WaterIsOverException() {
        super("Milk is over");
    }
}
