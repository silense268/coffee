package com.example.testITT.rest.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Getter
@Setter
public class CoffeeRequest {

    @Min(value = 0, message = "waterConsumption should not be less than 0")
    @Max(value = 100, message = "waterConsumption should not be greater than 100")
    private int waterConsumption;

    @Min(value = 0, message = "milkConsumption should not be less than 0")
    @Max(value = 100, message = "milkConsumption should not be greater than 100")
    private int milkConsumption;

    @Min(value = 0, message = "coffeeConsumption should not be less than 0")
    @Max(value = 100, message = "coffeeConsumption should not be greater than 100")
    private int coffeeConsumption;
}

